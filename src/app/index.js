import React from 'react';
import {render } from 'react-dom';
import { BrowserRouter, Route, browserHistory, IndexRoute } from 'react-router-dom';
import Root from './components/root';
import Home from './components/home';
import User from './components/user';

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Route path={"/"} component={Root}>
                    <div>
                        {/* <IndexRoute component={Home} /> */}
                        <Route path={"user"} component={User}/>
                        <Route path={"home"} component={Home}/>
                    </div>
                </Route>
            </BrowserRouter>
        )
    }
}

render(<App />, window.document.getElementById("app"));